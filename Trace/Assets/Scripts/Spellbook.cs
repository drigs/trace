﻿using UnityEngine;
using System.Collections;
using System;

public class Spellbook : MonoBehaviour, ItemInterface
{
    static Spellbook instance;
    static public Spellbook Instance { get { return instance; } }
    static Button[] buttons;
    static string spell;
    static public bool active;
    static bool flipping;
    static bool right;
    static float rotated;
    static float rotateValue = 175;

    void Awake()
    {
        instance = this;
        buttons = GetComponentsInChildren<Button>();
        SetActive(false);
    }

    static public void AddRune(string runeName)
    {
        spell += runeName;
    }

    static public void RemoveRune(string runeName)
    {
       SpellManager.CheckName(runeName, ref spell);
    }

    void Update()
    {
        if (active)
        {
            if (!flipping)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    right = true;
                    flipping = true;
                    rotated = 0;
                }

                if (Input.GetKeyDown(KeyCode.Q))
                {
                    right = false;
                    flipping = true;
                    rotated = 0;
                }
            }

            if (spell != null)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    MainUseItem();
                }
            }

            if (flipping)
            {
                FlipPage();
            }
        }
    }

    static public void SetActive(bool active)
    {
        Spellbook.active = active;
        instance.gameObject.SetActive(active);
    }

    static public void FlipPage()
    {
        float valueToRotate = rotateValue * Time.deltaTime;

        if (rotated + valueToRotate >= 90)
        {
            valueToRotate = 90 - rotated;
            flipping = false;
        }

        instance.transform.Rotate(Vector3.up, (right ? 1 : -1)*valueToRotate);
        rotated += valueToRotate;        
    }

    public void MainUseItem()
    {
        if (spell != null)
        {
            SpellManager.Instance.SetSpell(spell);

            foreach (Button button in buttons)
            {
                if (button.selected) button.Deselect();
            }

            spell = null;
        }
    }

    public void SecondaryUseItem()
    {
    }
}
