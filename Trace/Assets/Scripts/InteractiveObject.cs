﻿using UnityEngine;

public class InteractiveObject : MonoBehaviour
{
    [SerializeField] bool isMovable;
    [SerializeField] protected float weight = 1;
    [SerializeField] protected float moveMultiplier = 1;
    [SerializeField] protected bool startsFloating;
    [SerializeField] bool isScalable;
    [SerializeField] protected int scaleMultiplier = 1;
    [SerializeField] bool isLightable;
    [SerializeField] protected Vector3 lightPosition;
    [SerializeField] protected LightType lightType = LightType.Point;
    [SerializeField] protected bool isLit;
    [SerializeField] bool isLightSensitive;
    [SerializeField] bool isFadable;
    protected AudioSource audioSource { get; private set; }
    protected Transform player;
    protected Color color;
    protected Vector4 vectorColor;
    protected float time;
    protected bool acting;


    protected void Start()
    {
        player = Player.Instance.transform;
        vectorColor = Color.black;
        InteractiveObject[] ios = GetComponents<InteractiveObject>();
        foreach (InteractiveObject obj in ios)
        {
            vectorColor += ColorManager.GetColor(obj.GetType(), true);
        }
        vectorColor = vectorColor / ios.Length;
        vectorColor.w = 1f;
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            renderer.material.color = vectorColor;
        }

        if (GetType() == typeof(InteractiveObject))
            Destroy(this);
    }

    private void SetAtributes (InteractiveObject obj)
    {
        obj.weight = weight;
        obj.scaleMultiplier = scaleMultiplier;
        obj.lightPosition = lightPosition;
        obj.lightType = lightType;
        obj.isLit = isLit;
        obj.moveMultiplier = moveMultiplier;
        obj.startsFloating = startsFloating;
    }

    private void InitiateClasses()
    {
        if (isMovable)
        {
            gameObject.AddComponent<Movable>();
            SetAtributes(GetComponent<Movable>());     
        }
        if (isScalable)
        {
            gameObject.AddComponent<Scalable>();
            SetAtributes(GetComponent<Scalable>());
        }
        if (isLightable)
        {
            gameObject.AddComponent<Lightable>();
            SetAtributes(GetComponent<Lightable>());
        }
        if (isLightSensitive)
        {
            gameObject.AddComponent<LightSensitive>();
            SetAtributes(GetComponent<LightSensitive>());
        }
        if (isFadable)
        {
            gameObject.AddComponent<Fadable>();
            SetAtributes(GetComponent<Fadable>());
        }
    }

    protected void Initialize()
    {
        audioSource = gameObject.AddComponent<AudioSource>();

        if (GetComponent<Renderer>())
        {
            vectorColor = GetComponent<Renderer>().material.color;
        }
        else
        {
            vectorColor = Color.black;
        }
    }

    private void Awake()
    {
        InitiateClasses();       
    }

    #region Tests
    public bool TestUpdate()
    {
        acting = false;
        TestMovable();
        
        return acting; 
    }

    private void TestFadable()
    {
        if (GetComponent<Fadable>())
        {
            acting = GetComponent<Fadable>().fading;
        }
    }

    private void TestScalable()
    {
        if (GetComponent<Scalable>())
        {
            acting = GetComponent<Scalable>().scaling;
            if (!acting)
                TestFadable();
        }
    }

    private void TestMovable()
    {
        if (GetComponent<Movable>())
        {
            acting = ((GetComponent<Movable>().floating) && (!GetComponent<Movable>().startsFloating));
            if (!acting)
            {
                acting = GetComponent<Movable>().moving;
            }
        }
        if (!acting)
            TestScalable();
    }
    #endregion
}
