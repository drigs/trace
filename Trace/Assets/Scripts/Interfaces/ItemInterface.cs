﻿using UnityEngine;
using System.Collections;

public interface ItemInterface
{
    void MainUseItem();
    void SecondaryUseItem();
}
