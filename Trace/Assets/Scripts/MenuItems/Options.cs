﻿using UnityEngine;
using System.Collections;
using System;

public class Options : UIObject
{
    [SerializeField]GameObject menu;
    [SerializeField]GameObject optionsMenu;

    public override void Execute()
    {
        optionsMenu.SetActive(!optionsMenu.activeSelf);
        menu.SetActive(!menu.activeSelf);
    }

}
