﻿using UnityEngine;
using System.Collections;
using System;

public class Quit : UIObject
{
    public override void Execute()
    {
        Application.Quit();
    }
}
