﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class UIObject  : MonoBehaviour {
    [SerializeField]public GameObject selection;
    public GameObject menuManager;
    abstract public void Execute();

    void Awake()
    {
        menuManager = GameObject.Find("MenuManager");
    }

    public void OnMouseEnter(BaseEventData anEvent)
    {
        Debug.Log("estou aqui");
        menuManager.GetComponent<MenuManager>().SelectItem(this);
    }

    public void OnMouseClick(BaseEventData anEvent)
    {
        Execute();
    }

}
