﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class NewGame : UIObject
{
    public override void Execute()
    {
        SceneManager.LoadScene(1);
        //Starts new game
    }
}
