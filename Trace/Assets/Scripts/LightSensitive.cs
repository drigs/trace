﻿using UnityEngine;
using System.Collections;
using System;

public class LightSensitive : InteractiveObject
{
    public bool hasToFade;
    private bool isFaded;
    private bool alreadyRan;
    protected Material material;


    protected void Awake()
    {
        Initialize();
        material = GetComponent<Renderer>().material;
        GetComponent<Collider>().isTrigger = true;
    }

    void Update()
    { 
        if (hasToFade)
        {
            if (!isFaded)
            {
                if (alreadyRan)
                {
                    AudioManager.Play("cast_missile", audioSource, 1, 1f);
                }
                alreadyRan = true;
            }
            Rendering.SetRenderingMode(material, RenderingMode.Fade);
            color = material.color;
            color.a = 0f;
            material.color = color;
            GetComponent<Collider>().isTrigger = true;
            isFaded = true;
        }
        else
        {
            if (isFaded)
            {
                if (alreadyRan)
                {
                    AudioManager.Play("lumos", audioSource, 1, 1f);
                }
                alreadyRan = true;
            }
            Rendering.SetRenderingMode(material, RenderingMode.Opaque);
            color = material.color;
            color.a = 1f;
            material.color = color;
            GetComponent<Collider>().isTrigger = false;
            isFaded = false;
        }
        
        hasToFade = false;
    }

    void OnTriggerEnter(Collider other)
    {
        RunCollision(other);
    }

    void OnTriggerStay(Collider other)
    {
        RunCollision(other);
    }

    void RunCollision(Collider other)
    {
        LightSource lightSource = other.gameObject.GetComponent<LightSource>();
        if (lightSource)
        {
            hasToFade = true;
        }
    }

}
