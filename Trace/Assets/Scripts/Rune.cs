﻿using UnityEngine;
using UnityEngine.UI;
public class Rune : MonoBehaviour
{
    public RuneID id;
    public bool isInitialized = false;


    void Update()
    {
        if (!isInitialized)
        {
            foreach (GameObject button in FindObjectOfType<RuneManager>().buttons)
            {
                if (id == button.GetComponent<Button>().id)
                {

                    if (GetComponent<Renderer>().material)
                    {
                        GetComponent<Renderer>().material.mainTexture =
                        Resources.Load<Texture>("Materials/Runes/" +
                        button.GetComponent<Image>().sprite.name);
                    }
                    isInitialized = true;
                    break;
                }
            }

        }
    }
}