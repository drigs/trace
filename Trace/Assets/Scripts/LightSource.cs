﻿using UnityEngine;
using System.Collections;
using System;

public class LightSource : MonoBehaviour
{
    new Light light;

    void Awake()
    {
        light = GetComponent<Light>();
    }

    void Update()
    {
        if (light != null)
            CheckLightCollision();
    }    

    private void CheckLightCollision()
    {
        //if (Vector3.Distance(light.transform.position, transform.position) < 2)
        //    return true;
        if (light.type == LightType.Spot)
        {
            CheckCollisionSpot();
        }
        else if (light.type != LightType.Directional)
        {
            //CheckCollisionRadius();
        }
    }

    private void CheckCollisionRadius()
    {
        CheckAllCornersRadius();
    }

    private void CheckCollisionSpot()
    {
        CheckAllCornersSpot();
    }

    private void CheckAllCornersRadius()
    {
        CheckAllCorners((float)Math.PI, new Vector3[]{ transform.forward, -transform.forward});
    }

    private void CheckAllCorners(float angle, Vector3[] startPoint)
    {
        for (int j = 0; j < startPoint.Length; j++)
        {
            for (float i = 0; i < (float)Math.PI / 2; i += 0.1f)
            {
                CheckDirection(Vector3.RotateTowards(startPoint[j], Vector3.RotateTowards(transform.up, transform.right, i, 0.0f),
                                                     angle, 0.0f), startPoint[j], angle);
                CheckDirection(Vector3.RotateTowards(startPoint[j], Vector3.RotateTowards(transform.up, -transform.right, i, 0.0f),
                                                     angle, 0.0f), startPoint[j], angle);
                CheckDirection(Vector3.RotateTowards(startPoint[j], Vector3.RotateTowards(-transform.up, transform.right, i, 0.0f),
                                                     angle, 0.0f), startPoint[j], angle);
                CheckDirection(Vector3.RotateTowards(startPoint[j], Vector3.RotateTowards(-transform.up, -transform.right, i, 0.0f),
                                                     angle, 0.0f), startPoint[j], angle);
            }
            RaycastHit hit;
            //Debug.DrawRay(light.transform.position,
            //                    startPoint[j] * light.range, Color.red);
            if (Physics.Raycast(light.transform.position,
                                    startPoint[j], out hit, light.range))
            {
                LightSensitive obj = hit.collider.gameObject.GetComponent<LightSensitive>();
                if (obj)
                {
                    obj.hasToFade = true;
                }
            }
        }
    }

    private void CheckAllCornersSpot()
    {
        CheckAllCorners((float)Math.PI / 360 * light.spotAngle, new Vector3[] { transform.forward });
    }

    private void CheckDirection(Vector3 origin, Vector3 destination, float angle)
    {
        for (float i = 0; i <= angle; i += 0.1f)
        {
            Vector3 newDir = Vector3.RotateTowards(origin, destination, i, 0.0f);
            RaycastHit hit;
            //Debug.DrawRay(light.transform.position,
            //                newDir * light.range, Color.red);
            if (Physics.Raycast(light.transform.position,
                                newDir, out hit, light.range))
            {
                LightSensitive obj = hit.collider.gameObject.GetComponent<LightSensitive>();
                if (obj)
                {
                    obj.hasToFade = true;
                }
            }
        }
    }
}
