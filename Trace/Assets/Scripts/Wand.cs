﻿using UnityEngine;
using System.Collections;

public class Wand : MonoBehaviour, ItemInterface
{
    public float force { get; private set; }
    public string spellName { get; private set; }
    public Transform point;

    public void ChargeSpell(string name, float f)
    {
        spellName = name;
        force = f;
    }

    public void LightWand()
    {
        Player.Instance.LightWand();
    }

    public void ShootSpell()
    {
        Player.Instance.ShootSpell(spellName, force);
    }


    #region ItemInterface Functions
    public void MainUseItem()
    {
        SpellManager.Instance.RecastSpell();
    }

    public void SecondaryUseItem()
    {
        GameManager.Instance.TogglePanel();
    }
    #endregion
}
