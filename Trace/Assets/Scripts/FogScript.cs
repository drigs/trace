﻿using UnityEngine;
using System.Collections;

public class FogScript : MonoBehaviour {

    Transform player;

    void Start()
    {
        player = Player.Instance.transform;
    }

	// Update is called once per frame
	void Update () {
        transform.position = player.position;
	}
}
