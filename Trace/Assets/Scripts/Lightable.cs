﻿using UnityEngine;
using System.Collections;

public class Lightable : InteractiveObject {
    new protected Light light;
    protected LightSource lightSource;
    private float rangeModifier;

    protected void Awake()
    {
        Initialize();

        if (GetComponentInChildren<Light>())
        {
            light = GetComponentInChildren<Light>();
            light.enabled = isLit;
            rangeModifier = light.range;
            ConjureLight(1);
        }
        else
        {
            rangeModifier = 7;
        }
        
    }

    new protected void Start()
    {
        base.Start();
    }

    public void RemoveLight(bool playSound = true)
    {
        //Light light = GetComponentInChildren<Light>();
        //Debug.Log("entrei aqui no remove");
        if (light != null)
        {
            if (lightSource == null)
            {
                lightSource = GetComponentInChildren<LightSource>();
            }
            lightSource.enabled = false;
            light.enabled = false;
            isLit = false;
            //play sound
        }
    }

    public void ConjureLight(float force, bool playSound = true)
    {
        //Debug.Log("entrei aqui no conjur");
        if (!isLit && light == null)
        {
            light = (Light)Instantiate(Resources.Load<Light>("Prefabs/" + lightType + " Light"), transform.position, Quaternion.identity);
            light.gameObject.AddComponent<LightSource>();
            light.transform.SetParent(transform);
            light.transform.position = transform.TransformPoint(lightPosition);
            light.transform.forward = transform.forward;            //Setting direction
            light.transform.Rotate(Vector3.right * 90, Space.Self); //to down
            light.range *= force;
            isLit = true;
            if (playSound) AudioManager.Play("lumos", 1, 1f / force);
        }
        else
        {
            light.enabled = true;
            if (lightSource == null)
            {
                lightSource = GetComponentInChildren<LightSource>();
            }
            if (lightSource != null)
            {
                lightSource.enabled = true;
            }
            if (light.range != rangeModifier * force)
            {
                light.range = rangeModifier * force;
                if (playSound) AudioManager.Play("lumos", 1, 1f / force);
            }
        }
    }

}
