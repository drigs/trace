﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Button : MonoBehaviour {
    public RuneID id;
    public KeyCode shortcut;
    public bool selected { set; get; }

    new Light light;
    RuneManager rm;

    void Start()
    {
        light = GetComponentInChildren<Light>();
        rm = GameObject.Find("Rune Manager").GetComponent<RuneManager>();
        shortcut = (KeyCode)(transform.GetSiblingIndex() + 49);
    }

    void Update()
    {        
       if (Input.GetKeyDown(shortcut))
            GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
    }

    public void Deselect()
    {
        selected = false;
        light.intensity = 1.5f;
        light.enabled = false;
        Spellbook.RemoveRune(id.ToString());
    }

    //public void AddRune()
    //{
    //    rm.AddRune(gameObject);
    //}

    void OnMouseOver()
    {
        light.enabled = true;
        if (Input.GetMouseButtonDown(0))
        {
            if (selected)
            {
                Deselect();
            }
            else
            {
                if (id != RuneID.Si)
                {
                    Spellbook.AddRune(id.ToString());
                }
                else
                {
                    Spellbook.Instance.MainUseItem();
                }
                selected = true;
            }
        }
    }

    void OnMouseExit()
    {
        if (!selected)
        {
            light.intensity = 1.5f;
            light.enabled = false;
            //selected = false;
        }
    }
}
