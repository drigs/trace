﻿using UnityEngine;
using System.Collections;

public class Movable : InteractiveObject
{
    public bool floating;
    public bool moving;
    protected float initialWeight;
    new protected Collider collider;
    protected CharacterController cc;
    protected Vector3 pos0;
    protected Vector3 previousPosition = Vector3.zero;
    protected Vector3 destination;
    protected Vector3 direction;
    protected float force = 1;
    protected float speed = 1;
    protected float distance;
    protected bool peaked;
    protected bool falling;
    bool useGravity;

    const RigidbodyConstraints FREEZE_MINUS_Y = (RigidbodyConstraints)122;

    RaycastHit hit;
    protected void Awake()
    {
        Initialize();
        initialWeight = weight;
        collider = GetComponent<Collider>();
    }

    new void Start()
    {
        base.Start();
        useGravity = !startsFloating;
    }

    public void FixedUpdate()
    {
        if (floating)
        {
            Float();
        }
        else
        {
            if (moving)
            {
                Move();
            }
        }
        ApplyGravity();
    }

    void ApplyGravity()
    {
        if (useGravity)
        {
            if (!collider.isTrigger || GetComponent<LightSensitive>())
            {
                RaycastHit hit = new RaycastHit();
            
                if (!Physics.BoxCast(collider.bounds.center, collider.bounds.extents, Vector3.down, out hit, Quaternion.identity, 0.05f))
                    //(collider.bounds.center, Vector3.down, out hit, collider.bounds.extents.y + 0.000001f))
                {
                    transform.Translate(Vector3.down * speed * Time.deltaTime, Space.World);
                }
                else
                {
                    //transform.position = new Vector3(transform.position.x,(hit.collider.transform.position.y - hit.collider.bounds.extents.y - 0.1f), transform.position.z);
                }
            }
        }
        
    }

    public void Move(char dir, int f, Transform sourceObject)
    {
        force = f;
        speed = force / weight;
        Vector3 source = (transform.position - sourceObject.position).normalized;
        if (speed >= 1)
        {
            speed /= weight + 1;
            float pitch = speed;
            speed *= moveMultiplier;
            distance = force * 2 / weight * moveMultiplier;
            pos0 = transform.position;
            switch (dir)
            {
                case 'f':
                    if (Mathf.Abs(source.z) > Mathf.Abs(source.x))
                    {
                        direction = Vector3.forward * source.z;
                    }
                    else
                    {
                        direction = Vector3.right * source.x;
                    }
                    if (CanMove())
                    {
                        moving = true;
                        AudioManager.Play("summon_complete1", pitch: pitch);
                    }
                    break;
                case 'b':
                    if (Mathf.Abs(source.z) > Mathf.Abs(source.x))
                        direction = Vector3.back * source.z;
                    else
                        direction = Vector3.left * source.x;
                    if (CanMove())
                    {
                        moving = true;
                        AudioManager.Play("summon_complete1", pitch: pitch);
                    }
                    break;
                case 'u':
                    direction = Vector3.up;
                    if (CanMove())
                    {
                        floating = true;
                        AudioManager.Play("summon_complete1", pitch: pitch);
                    }
                    break;

                default:
                    break;
            }
            destination = pos0 + (direction * distance);
        }
    }

    void Float()
    {
        if (transform.position.y < pos0.y + distance && CanMove() && !peaked)
        {
            if (!startsFloating)
            {
                useGravity = false;
                //rb.constraints = FREEZE_MINUS_Y;
            }
            transform.Translate(Vector3.up * speed * Time.deltaTime, Space.World);
            if (previousPosition != transform.position)
            {
                previousPosition = transform.position;
            }
            else
            {
                time = Time.time;
                peaked = true;
            }
        }
        else
        {
            if (!peaked)
            {
                time = Time.time;
                peaked = true;
            }
            else if (Time.time >= time + 5)
            {
                if (!startsFloating)
                {
                    useGravity = true;
                    if (Physics.Raycast(transform.position, Vector3.down, collider.bounds.extents.y + 0.1f))
                    {
                        if (false)//transform.position.y <= pos0.y)
                        {
                            Vector3 newPos = transform.position;
                            newPos.y = pos0.y;
                            transform.position = newPos;
                            floating = false;
                            useGravity = false;
                            //rb.constraints = RigidbodyConstraints.FreezeAll;
                            FinishMovement();
                        }
                        FinishMovement();
                    }
                }
                else
                {
                    if (transform.position.y - speed * Time.deltaTime <= pos0.y)
                    {
                        //rb.constraints = RigidbodyConstraints.FreezeAll;
                        //transform.position = pos0;
                        floating = false;
                        FinishMovement();
                    }
                    else
                    {
                        transform.Translate(Vector3.down * speed * Time.deltaTime, Space.World);
                    }
                }

            }
        }
    }

    public void Move()
    {
        //Debug.Log(Vector3.Distance(transform.position, destination));
        //Debug.Log(speed * Time.deltaTime);
        CheckFalling();
        float distance = Vector3.Distance(transform.position, destination);
        if (distance > .05f)
        {
            if (CanMove())
            {
                transform.Translate(direction * speed * Time.deltaTime, Space.World);
                if (previousPosition != transform.position)
                {
                    previousPosition = transform.position;
                }
                else
                {
                    moving = false;
                }
            }
            else
            {
                moving = false;
            }
        }
        else
        {
            moving = false;
        }
    }

    protected void FinishMovement()
    {
        //rb.constraints = RigidbodyConstraints.FreezeAll;
        moving = false;
        peaked = false;
        floating = false;
        falling = false;
        force = 0;
        useGravity = !startsFloating;
    }

    bool CanMove()
    {
        return !Physics.BoxCast(transform.position, collider.bounds.extents, direction, out hit,
                                Quaternion.identity, 0.05f);
    }

    void CheckFalling()
    {
        bool aux = Physics.Raycast(transform.position, Vector3.down, collider.bounds.extents.y + 0.1f);
        if (!falling)
        {
            falling = !aux;
        }
        else
        {
            if (aux)
            {
                FinishMovement();
            }
        }
    }
}
