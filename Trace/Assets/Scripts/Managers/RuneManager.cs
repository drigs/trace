﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class RuneManager : MonoBehaviour
{
    [SerializeField]GameObject castButtons;
    [SerializeField]GameObject recastButtons;
    [SerializeField]GameObject castEdit;
    [SerializeField]GameObject runesGroup;
    [SerializeField]GameObject buttonGroup;
    [SerializeField]GameObject buttonPrefab;
    [SerializeField]Sprite stdImage;

    public Button[] buttonComponents;
    public GameObject[] buttons;
    public Sprite[] sprites;
    public List<RuneID> ids = new List<RuneID>();
    public GameObject[] runeObjs;

    Image[] runeImgs;
    int numMaxRunes;
    int count;

    public void Start()
    {
        buttonComponents = buttonGroup.GetComponentsInChildren<Button>();
        buttons = new GameObject[buttonComponents.Length];
        sprites = Resources.LoadAll<Sprite>("Runes/LoL Runes");
        runeImgs = runesGroup.GetComponentsInChildren<Image>();
        numMaxRunes = runeImgs.Length;
        bool isDebug = false;
        //isDebug = true; //<--uncomment to cheat
        //Initializing random rune IDs and sprites
        for (int i = 0; i < sprites.Length; i++)
            ids.Add((RuneID)i);            
        int c = 0;
        buttons = new GameObject[sprites.Length];
        foreach (Sprite spr in sprites)
        {            
            int runeId = Random.Range(0, ids.Count);
            GameObject button = Instantiate(buttonPrefab);
            button.transform.SetParent(buttonGroup.transform,false);
            button.GetComponent<Image>().sprite = spr;
            button.GetComponent<Button>().id = ids[runeId];
            button.GetComponentInChildren<Text>().text = ids[runeId].ToString();
            if (!isDebug)
                button.GetComponentInChildren<Text>().gameObject.SetActive(false); 
            buttons[c] = button.gameObject;
            ids.RemoveAt(runeId);
            c++;
        }
        foreach (Image rune in runeImgs)
        {
            rune.sprite = stdImage;
        }

        ids.Clear();
        SetRunes();
        runesGroup.SetActive(false);
        buttonGroup.SetActive(false);
        castButtons.SetActive(false);
        recastButtons.SetActive(false);
        if (!isDebug)
            castEdit.SetActive(false);
    }

    //Called by rune button
    public void AddRune(GameObject button)
    {
        if (count < numMaxRunes)
        {
            RuneID newRune = button.GetComponent<Button>().id;
            if (!ids.Contains(newRune))
            {
                ids.Add(newRune);
                //runeImgs[count].GetComponent<Image>().sprite = button.GetComponent<Image>().sprite;
                SpellManager.AddRune(newRune);
                count++;
                castButtons.SetActive(true);
                recastButtons.SetActive(true);
            }
        }
    }

    public void ResetRunes()
    {
        SpellManager.RemoveAllRunes();
        for (int i = 0; i < runeImgs.Length; i++)
        {
            runeImgs[i].GetComponent<Image>().sprite = stdImage;
            count = 0;
            ids.Clear();
        }
        castButtons.SetActive(false);
        castButtons.SetActive(true);
    }

    void SetRunes()
    {
        Rune[] runes = FindObjectsOfType<Rune>();
        runeObjs = new GameObject[runes.Length];
        for (int i = 0; i < runes.Length; i++)
        {
            runeObjs[i] = runes[i].gameObject;

            foreach (GameObject button in buttons)
            {
                if (runes[i].id == button.GetComponent<Button>().id)
                {
                    
                    if (runeObjs[i].GetComponent<Renderer>().material) {
                        runeObjs[i].GetComponent<Renderer>().material.mainTexture =
                                    Resources.Load<Texture>("Materials/Runes/" +
                                    button.GetComponent<Image>().sprite.name);
                    }

                    break;
                }
            }
        }
    }
}
