﻿using UnityEngine;
using System.Collections;

public class ColorManager : MonoBehaviour {
    private static Vector4 fadable = Color.blue;
    private static Vector4 scalable = Color.green;
    private static Vector4 movable = Color.red;
    private static Vector4 lightable = Color.yellow;
    private static Vector4 lightSensitive = Color.magenta;
    private static Vector4 emptySpell = Color.grey;

    public static System.Type GetTypeOfSpell(string spellName)
    {
        if (SpellManager.CheckName("Mo", ref spellName, false))
        {
            return typeof(Movable);
        }
        if (SpellManager.CheckName("TraOn", ref spellName, false))
        {
            return typeof(Scalable);
        }
        if (SpellManager.CheckName("TraTw", ref spellName, false))
        {
            return typeof(Fadable);
        }
        if (SpellManager.CheckName("ConOn", ref spellName, false))
        {
            return typeof(Lightable);
        }
        return typeof(InteractiveObject);

    }

    public static Vector4 GetColor(System.Type type, bool divided = false)
    {
        Vector4 color = Color.white;
        if (type == typeof(Fadable)) {
            color = fadable;
        }
        else if (type == typeof(Scalable))
        {
            color = scalable;
        }
        else if (type == typeof(Movable))
        {
            color = movable;
        }
        else if (type == typeof(Lightable))
        {
            color = lightable;
        }
        else if (type == typeof(LightSensitive))
        {
            color = lightSensitive;
            divided = false;
        }
        else if (type == typeof(InteractiveObject))
        {
            color = emptySpell;
        }
        if (divided)
        {
            color /= 4f;
        } 
        else
        {
            color /= 2;
        }
        return color;        
    }
}
