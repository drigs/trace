﻿using UnityEngine;
using System.Collections;

public class ItemManager : MonoBehaviour {
    public ItemInterface wand;
    public ItemInterface spellbook;
    void Start()
    {
        wand = Player.Instance.wand;
        spellbook = Spellbook.Instance;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) || Input.GetMouseButtonDown(0))
        {
            if (!GameManager.PanelIsActive)
            {
                if (wand != null)
                {
                    wand.MainUseItem();
                }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (wand != null)
            {
                wand.SecondaryUseItem();
            }
        }
    }
}
