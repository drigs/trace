﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {
    [SerializeField] UIObject newGame;
    [SerializeField] UIObject loadGame;
    [SerializeField] UIObject options;
    [SerializeField] UIObject quit;
    Color colorSelected;
    Color colorNotSelected;
    public UIObject[] menus;
    public int selectedMenu;
    float lastAxis;
    const float Cooldown = 0.125f;
    const float SelectionDelay = 0.1f;
    float coolDownDown;
    float coolDownUp;


    void Start()
    {
        menus = new UIObject[4] { newGame, loadGame, options, quit};
        selectedMenu = 0;
        colorSelected = newGame.GetComponent<Text>().color;
        colorNotSelected = loadGame.GetComponent<Text>().color;
        coolDownDown = Time.time - 5;
        coolDownUp = Time.time - 5;
        lastAxis = Input.GetAxis("Vertical");
    }

    void Update()
    {
        float input = Input.GetAxis("Vertical");
        if (input < -1*SelectionDelay)
        {
            if (lastAxis >= input)
            {
                if (Time.time - Cooldown > coolDownDown)
                {
                    incSelectedMenu(1);
                    coolDownDown = Time.time;
                }
            }
        }
        if (input > SelectionDelay)
        {
            if (lastAxis <= input)
            {
                if (Time.time - Cooldown > coolDownUp)
                {
                    incSelectedMenu(-1);
                    coolDownUp = Time.time;
                }
            }
        }
        lastAxis = input;
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))  
        {
            menus[selectedMenu].Execute();
        }
    }

    void incSelectedMenu(int increment)
    {
        SelectItem(selectedMenu + increment);        
    }

    void SelectItem(int item)
    {
        menus[selectedMenu].selection.SetActive(false);
        menus[selectedMenu].GetComponent<Text>().color = colorNotSelected;
        selectedMenu = item;
        if (selectedMenu > 3)
        {
            selectedMenu = 0;
        }
        if (selectedMenu < 0)
        {
            selectedMenu = 3;
        }
        menus[selectedMenu].GetComponent<Text>().color = colorSelected;
        menus[selectedMenu].selection.SetActive(true);
        AudioManager.Play("select1",1,2);
    }

    public void SelectItem(UIObject item)
    {
        for (int i = 0; i < menus.Length; i++)
        {
            if (menus[i] == item)
            {
                SelectItem(i);  
            }
        }
    }


}

