﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

    static public AudioSource[] audioSources;
    static AudioClip[] clips;
    static GameObject gameObj;
    static string clipName;
    static float volume, pitch;

    void Awake()
    {
        gameObj = gameObject;
        clips = Resources.LoadAll<AudioClip>("Audio");
        audioSources = GetComponents<AudioSource>();
    }

    static public void Play(string clipName, float volume = 1, float pitch = 1)
    {
        AudioManager.clipName = clipName;
        AudioManager.volume = volume;
        AudioManager.pitch = pitch;

        bool found = false;
        foreach (AudioSource source in audioSources)
        {
            if (!source.isPlaying)
            {
                found = true;
                PlayClip(source);
            }
        }
        if (!found)
        {
            PlayClipTemp(gameObj);
        }
    }

    static void PlayClip(AudioSource source)
    {
        foreach (AudioClip clip in clips)
        {
            if (clip.name == clipName)
            {
                //Debug.Log("playing " + clipName);
                source.volume = volume;
                source.clip = clip;
                source.pitch = pitch;
                source.Play();
            }
        }        
    }

    //Plays clip on a temporary AudioSource
    static void PlayClipTemp(GameObject go)
    {
        AudioSource source = go.AddComponent<AudioSource>();
        foreach (AudioClip clip in clips)
        {
            if (clip.name == clipName)
            {
                //Debug.Log("playing " + clipName);
                source.volume = volume;
                source.clip = clip;
                source.pitch = pitch;
                source.Play();
                Destroy(source, clip.length);
            }
        }
    }

    static public void Play(string clipName, AudioSource audioSource, float volume, float pitch)
    {
        AudioManager.clipName = clipName;
        AudioManager.volume = volume;
        AudioManager.pitch = pitch;

        if (!audioSource.isPlaying)
        {
            PlayClip(audioSource);
        }
        else if (audioSource.clip.name != clipName)
        {
            PlayClipTemp(audioSource.gameObject);
        }
    }
}
