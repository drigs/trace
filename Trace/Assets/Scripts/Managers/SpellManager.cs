﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class SpellManager : MonoBehaviour
{
    static List<RuneID> runes = new List<RuneID>();
    public static List<InteractiveObject> selectedObjs = new List<InteractiveObject>();

    GameManager gameManager;

    static SpellManager instance;
    public static SpellManager Instance { get { return instance; } }

    string spellName = "";
    string spellCasted = "";
    static public string lastSpell = "";

    float cooldown;
    float castTime;
    int force = 1;

    bool messageShown;
    bool secondMessageShown;
    float timeSecondMessage;

    #region Rune List Management
    static public void AddSelectedObj(InteractiveObject io)
    {
        selectedObjs.Add(io);
    }
    static public void RemoveSelectedObj(InteractiveObject io)
    {
        selectedObjs.Remove(io);
    }
    static public void AddRune(RuneID r)
    {
        runes.Add(r);
    }

    static public void RemoveRune(RuneID r)
    {
        runes.Remove(r);
    }

    static public void RemoveAllRunes()
    {
        runes.Clear();
    }
    #endregion

    #region Unity Functions
    void Awake()
    {
        instance = this;
        cooldown = 1;
        castTime = -1;
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }

    void Update()
    {
        if (messageShown && Time.time > timeSecondMessage + 5)
        {
            secondMessageShown = true;
            FindObjectOfType<GameManager>().ChangeMessage("Add shortcuts to spells by holding the CTRL key and pressing an F key\n" +
                                                          "You can add a description to the spell by typing in the text fields above");
        }
    }
    #endregion

    #region Spellcasting
    public void RecastSpell()
    {
        RemoveAllRunes();
        SetSpell(lastSpell); 
    }    
    public void SetSpell(string sName = "")
    {
        if (Time.time >= cooldown + castTime)
        {
            castTime = Time.time;
            if (sName == "")
            {
                for (int i = 0; i < runes.Count; i++)
                {
                    spellName += runes[i];
                }
            }
            else
            {
                spellName = sName;
            }
            //Debug.Log(spellName);
            if (spellName != "")
                spellCasted = spellName;

            //Checks the casted spell
            CastSpell();

            //Clearing selections
            force = 1;
            //if (spellName == "")
            {
                lastSpell = spellCasted;
            }
            spellName = "";
            selectedObjs.Clear();
            runes.Clear();
        }
        else
        {
            AudioManager.Play("wrong", pitch: 1.5f);
        }
    }
    void CastSpell() 
    {
        //Setting force bonus
        if (CheckName("Pa", ref spellName, false))
        {
            if (CheckNameEnd("PaPb", ref spellName, false))
                force += 2;
            else if (CheckNameEnd("Pa", ref spellName, false))
                force++;
            else
                CheckName("Pa", ref spellName, false);
        }

        //Whether player is the target
        if (CheckNameStart("Se", ref spellName, false))
        {
            CheckName("Pa", ref spellName);
            CheckName("Pb", ref spellName);
            //Blink forward
            if (CheckNameEnd("SeMoOn", ref spellName))
            {
                Player.Instance.Blink(force);
            }
            //Jump Up
            else if (CheckNameEnd("SeMoTw", ref spellName))
            {
                Player.Instance.Float(force);
            }
            // Remove light spell
            else if (CheckNameEnd("SeConOnNon", ref spellName))
            {
                Player.Instance.RemoveLight();
            }
            // Conjure light spell
            else if (CheckNameEnd("SeConOn", ref spellName))
            {
                Player.Instance.ConjureLight(force);
            }
        }//Whether an object is the target
        else if (CheckNameStart("Ob", ref spellName, false))
        {
            Player.Instance.wand.ChargeSpell(spellName, force);
            Player.Instance.wandAnim.speed = 1 + ((force - 1) * .6f);
            Player.Instance.wandAnim.Play("ObjectSpell");
            
        }//Whether the spell affects time
        else if (CheckNameStart("Ti", ref spellName, false))
        {
            CheckName("Pa", ref spellName);
            CheckName("Pb", ref spellName);
            if (CheckNameEnd("TiTraTw", ref spellName))
            {
                Player.Instance.SlowTime(force);
            }
        }
        if (!messageShown)
        {
            FindObjectOfType<GameManager>().ChangeMessage("Press R or Right Click to recast");
            timeSecondMessage = Time.time;
            messageShown = true;
        }
    }
    #endregion

    #region Spellname Operations    
    public static bool CheckName(string runeName, ref string name, bool delete = true)
    {
        if (name.Contains(runeName))
        {
            if (delete)
                name = name.Remove(name.IndexOf(runeName), runeName.Length);
            return true;
        }
        else
            return false;

    }
    public static bool CheckNameStart(string runeName, ref string name, bool delete = true)
    {
        if (!delete)
            return name.StartsWith(runeName);
        else
        {
            bool starts = (name.StartsWith(runeName));
            if (starts)
                name = name.Remove(name.IndexOf(runeName), runeName.Length);
            return starts;
        }
    }

    public static bool CheckNameEnd(string runeName, ref string name, bool delete = true)
    {
        if (!delete)
            return name.EndsWith(runeName);
        else
        {
            bool ends = (name.EndsWith(runeName));
            if (ends)
                name = name.Remove(name.IndexOf(runeName), runeName.Length);
            return ends;
        }
    }

    #endregion

}
