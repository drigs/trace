﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour
{
    static GameManager instance;
    static public GameManager Instance { get { return instance; } }
    [SerializeField]GameObject runesPanel;
    [SerializeField]GameObject player;
    [SerializeField]GameObject shortcutPanel;
    static bool panelIsActive;
    static public bool PanelIsActive { get { return panelIsActive; } }

    static GameObject[] images;
    SpellManager spellManager;
    MouseLook mouseLook;
    string currentMessage;
    float timerMessage;
    bool showMessage;
    bool messageShown;
    string[] fList = new string[12];
    private string lastMessage;
    private float widthMessage;

    public void ChangeMessage(string aMessage)
    {
        currentMessage = aMessage;
        timerMessage = Time.time;
        showMessage = false;
    }

    void Start()
    {
        instance = this;
        Light[] lights = FindObjectsOfType<Light>();
        foreach (Light light in lights)
        {
            if (light.type == LightType.Spot)
            {
                light.gameObject.AddComponent<LightSource>();
            }
        }
        ChangeMessage("MessageStarting");
        player.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(true);
        mouseLook = player.GetComponent<FirstPersonController>().m_MouseLook;
        spellManager = GameObject.Find("Spell Manager").GetComponent<SpellManager>();
        for (int i = 0; i < 12; i++)
            fList[i] = "";
    }

    void Update()
    {
        if (lastMessage == "MessageStarting")
            ChangeMessage("Right click to open the spell menu");
        if (Time.time > timerMessage + 5)
        {
            showMessage = false;
        }        

        for (int i = 282; i < 294; i++)
        {
            if (Input.GetKeyDown((KeyCode)i)) //F#
            {
                if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
                {
                    fList[i - 282] = SpellManager.lastSpell;
                    if (SpellManager.lastSpell != "")
                    {
                        GameObject shortcutKey = shortcutPanel.transform.Find("F" + (i - 281).ToString()).gameObject;
                        if (shortcutKey != null)
                        {
                            GameObject textAssigned = shortcutKey.transform.Find("TextAssigned").gameObject;
                            if (textAssigned != null)
                            {
                                textAssigned.SetActive(true); 
                            }
                        }
                    }
                }
                else
                {
                    string f = fList[i - 282];
                    if (f != "")
                        spellManager.SetSpell(f);
                }
            }            
        }        
            
        if (Input.GetMouseButtonDown(1))
        {
            if (!messageShown)
            {
                ChangeMessage("Click the runes to add them to the spell");
                messageShown = true;
            }
        }   
        
    }

    float GetSize(string message)
    {
        float currentSize = 0;
        float maxSize = 0;
        for (int i = 0; i < message.Length; i++)
        {
            if (message[i] == '\n')
            {
                if (currentSize > maxSize)
                    maxSize = currentSize;
                currentSize = 0;
            }
            else
            {
                CharacterInfo info;
                Font font = GUI.skin.font;
                font.GetCharacterInfo(message[i], out info);
                currentSize += info.advance;
            }
        }
        if (currentSize > maxSize)
            maxSize = currentSize;
        return maxSize;
    }

    void OnGUI()
    {
        if (showMessage)
        {
            if (lastMessage != currentMessage)
            {
                widthMessage = Screen.width / 2 - (GetSize(currentMessage) / 2);
            }
            lastMessage = currentMessage;
            float width = 0f;
            if (currentMessage != "MessageStarting")
                width = 600f;
            GUI.Label(new Rect(new Vector2 (widthMessage, 100), new Vector2(width, width)), currentMessage);
        }
    }

    public void TogglePanel()
    {
        runesPanel.SetActive(!runesPanel.activeSelf);
        panelIsActive = runesPanel.activeSelf;
        player.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(!panelIsActive);
        player.GetComponent<FirstPersonController>().m_MouseLook.UpdateCursorLock();
        Spellbook.SetActive(!Spellbook.active);
    }
}
