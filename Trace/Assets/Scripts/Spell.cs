﻿using UnityEngine;
using System.Collections;

public class Spell : MonoBehaviour
{
    AudioSource audioSource;
    Vector3 direction;
    new string name;
    float force;
    float speed;
    bool shooting;
    float timeRadius;
    bool castingAoe;
    Light lightAoe;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (shooting)
            Shoot();
        if (castingAoe)
        {
            if (timeRadius + 10 < Time.time)
            {
                if (lightAoe != null)
                {
                    Destroy(lightAoe);
                    castingAoe = false;
                }
            }    
        }
    }

    public void Shoot(string spellName, Vector3 dir, float f)
    {
        name = spellName;
        direction = dir;
        force = f;
        speed = force * 10;
        transform.SetParent(GameObject.Find("Scenery").transform, true);
        transform.localScale = Vector3.one;
        AudioManager.Play("cast_missile", audioSource, .3f, 1);
        ParticleSystem ps = GetComponent<ParticleSystem>();
        ps.startColor = ColorManager.GetColor(ColorManager.GetTypeOfSpell(spellName), false);
        shooting = true;
    }

    void Shoot()
    {
        transform.Translate(direction * speed * Time.deltaTime, Space.World);
        if (Vector3.Distance(transform.position, Player.Instance.transform.position) > 100f)
            Finish();
    }

    void Finish()
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        ps.Stop();
        GetComponent<Collider>().enabled = false;
        if (audioSource.isPlaying)
            Destroy(gameObject, audioSource.clip.length - audioSource.time);
        else if (ps.isPlaying)
            Destroy(gameObject, ps.duration);
        else
            Destroy(gameObject);
    }

    void OnCollisionEnter(Collision col)
    {
        ProcessCollision(col.collider);
    }


    void ProcessCollision(Collider col)
    {
        if (col.gameObject.layer != 1 << LayerMask.NameToLayer("Player"))
        {
            //Debug.Log("spell collided with " + col.name);
            AudioManager.Play("missile_hit", audioSource, 1, 1);
            shooting = false;
            if (col.gameObject.GetComponent<InteractiveObject>())
            {
                if (!col.gameObject.GetComponent<InteractiveObject>().TestUpdate())
                {
                    CastSpell(col.gameObject.GetComponent<InteractiveObject>());
                }
            }
            else if (name.Contains("Ra"))
            {
                CastSpell(null);
            }
            Finish();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        //ProcessCollision(col);
    }
    void CastSpell(InteractiveObject io)
    {
        SpellManager.lastSpell = name;
        SpellManager.CheckName("Pa", ref name);
        SpellManager.CheckName("Pb", ref name);
        if (SpellManager.CheckNameEnd("Ra", ref name))
        {//AOE Spells cast a radius around the collision and affect all targets
            lightAoe = (Light)Instantiate(Resources.Load<Light>("Prefabs/SpellRadiusLight"), transform.position, Quaternion.identity);
            float auxRange = lightAoe.range;
            lightAoe.transform.SetParent(transform);
            lightAoe.transform.position = transform.position;
            lightAoe.color = ColorManager.GetColor(ColorManager.GetTypeOfSpell(name), false);
            timeRadius = Time.time;
            castingAoe = true;
            string auxLastSpell = SpellManager.lastSpell;
            string auxSpell = name;
            Debug.Log(name);
            int layerMask = 1 << 8;
            Collider[] collisions = Physics.OverlapSphere(transform.position, auxRange, layerMask);
            for (int i = 0; i < collisions.Length; i++)
            {
                name = auxSpell;
                if (collisions[i].gameObject.GetComponent<InteractiveObject>())
                {
                    if (!collisions[i].gameObject.GetComponent<InteractiveObject>().TestUpdate()) { 
                        CastSpell(collisions[i].gameObject.GetComponent<InteractiveObject>());
                    }
                }
            }
            SpellManager.lastSpell = auxLastSpell;
            return;
        }
        // Growth spell
        if (SpellManager.CheckNameEnd("ObTraOn", ref name))
        {
            if (io.GetComponent<Scalable>())
            {
                io.GetComponent<Scalable>().Scale((int)force);
            }
        }//Fade spell
        else if (SpellManager.CheckNameEnd("ObTraTw", ref name))
        {
            if (io.GetComponent<Fadable>())
                io.GetComponent<Fadable>().Fade(force);
        }// Light spells
        else if (SpellManager.CheckNameEnd("ObConOnNon", ref name))
        {// Remove Light
            if (io.GetComponent<Lightable>())
                io.GetComponent<Lightable>().RemoveLight();
        }
        else if (SpellManager.CheckNameEnd("ObConOn", ref name))
        {//Conjure Light
            if (io.GetComponent<Lightable>())
                io.GetComponent<Lightable>().ConjureLight(force);
        }// Moving spells
        else if (SpellManager.CheckNameStart("ObMo", ref name, false))
        {
            //Push
            if (SpellManager.CheckNameEnd("ObMoOn", ref name))
            {
                if (io.GetComponent<Movable>())
                    io.GetComponent<Movable>().Move('f', (int)force, transform);
            }
            //Pull                       
            else if (SpellManager.CheckNameEnd("ObMoTw", ref name))
            {
                if (io.GetComponent<Movable>())
                    io.GetComponent<Movable>().Move('b', (int)force, Player.Instance.transform);
            }
            //Float
            else if (SpellManager.CheckNameEnd("ObMoTh", ref name))
            {
                if (io.GetComponent<Movable>())
                    io.GetComponent<Movable>().Move('u', (int)force, transform);
            }
        }
    }   
}
