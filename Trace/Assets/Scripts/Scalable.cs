﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scalable : Movable
{    
    Scalable[] scalables;
    Vector3 initialScale;
    float scaleSpeed;
    float targetScale;
    float previousScale;
    float scale = 1;
    public bool scaling;
    bool up;
    List<Collider> restColliders;


    new void Awake()
    {
        base.Awake();
        restColliders = new List<Collider>();
        CalculateRestColliders();
        transform.localScale.Normalize();
        initialScale = transform.localScale;
        previousScale = initialScale.magnitude;
        scale = initialScale.magnitude;
        vectorColor += ColorManager.GetColor(GetType(), true);
    }

    void CalculateRestColliders()
    {
        //if (!restColliders.Contains(col.collider))
        //{
        //    restColliders.Add(col.collider);
        //}
    }

    new void FixedUpdate()
    {
        base.FixedUpdate();
        if (scaling)
            Scale();
    }

    public void Scale(int force)
    {
        scaling = true;
        scalables = GetComponentsInChildren<Scalable>();
        foreach (Scalable scalable in scalables)
        {
            if (scalable != this)
            {
                scalable.enabled = false;
            }
        }        
        switch (force)
        {
            case 1:
                up = false;
                if (weight > 1) weight--;
                targetScale = initialScale.magnitude * .75f * scaleMultiplier;
                break;
            case 2:
                up = (scale < initialScale.magnitude);
                weight = initialWeight;
                targetScale = initialScale.magnitude;
                break;
            case 3:
                up = true;
                if (weight < 3) weight++;
                targetScale = initialScale.magnitude * 1.5f * scaleMultiplier;
                break;
            default:
                scaling = false;
                break;
        }
        scaleSpeed = force / 2f / scaleMultiplier;
    }

    void Scale()
    {
        float multiplier = up ? 1 : -1;
        if (multiplier * scale >= multiplier * targetScale)
        {
            Finish();
        }
        else
        {
            Vector3 direction = up ? Vector3.up : Vector3.down;
            float factor = scaleSpeed * Time.deltaTime;            
            transform.Translate(direction * factor / 2);
            transform.localScale += multiplier * Vector3.one * factor;
            scale += multiplier*(factor);
        }
    }

    public void OnCollisionEnter(Collision col)
    {
        if (scaling)
        {
            if ((!restColliders.Contains(col.collider)) && (!col.collider.gameObject.GetComponent<Spell>())) 
            {
                //Debug.Log(col.collider.gameObject);
                targetScale = previousScale;
                up = !up;
            }
        }
        else
        {
            if (!restColliders.Contains(col.collider))
            {
                //Debug.Log(col.collider.gameObject);
                restColliders.Add(col.collider);
            }
        }
    }

    public void Finish()
    {
        scaling = false;
        previousScale = scale;
        foreach (Scalable scalable in scalables)
        {
            if (scalable != this)
            {
                scalable.Finish();
                scalable.enabled = true;
            }
            else if (scalables.Length > 1)
                scalable.enabled = false;
        }
        CalculateRestColliders();
    }
}
