﻿using UnityEngine;
using System.Collections;
public class Fadable : InteractiveObject
{
    protected Material material;
    float fadeSpeed;
    float fadeAmount;
    bool faded;
    public bool fading;

    protected void Awake()
    {
        Initialize();
        material = GetComponent<Renderer>().material;
    }

    new void Start()
    {
        base.Start();
        color = vectorColor;
        color.a = 1f;
        material.color = color;
    }

    void Update()
    {
        if (fading) Fade();
    }

    public void Fade(float force)
    {
        if (GetComponent<Rigidbody>())GetComponent<Rigidbody>().useGravity = false;
        Rendering.SetRenderingMode(material, RenderingMode.Fade);
        fadeSpeed = force / 5;
        fadeAmount = 1 - (force / 3) + .01f;
        fading = true;
    }

    void Fade()
    {
        if (!faded)
        {
            if (color.a - fadeSpeed * Time.deltaTime >= fadeAmount)
            {
                color.a -= fadeSpeed * Time.deltaTime;
                GetComponent<Renderer>().material.color = color;
            }
            else
            {
                color.a = fadeAmount;
                GetComponent<Renderer>().material.color = color;
                faded = true;
                time = Time.time;
            }
        }
        else if (Time.time >= time + 5)
        {
            if (color.a < 1f)
            {
                color.a += fadeSpeed * Time.deltaTime;
                GetComponent<Renderer>().material.color = color;
            }
            else
            {
                Rendering.SetRenderingMode(material, RenderingMode.Opaque);
                fading = false;
                faded = false;
                if(GetComponent<Rigidbody>())
                    GetComponent<Rigidbody>().useGravity = true;
            }
        }
        GetComponent<Collider>().enabled = (color.a > .01f);
    }
}
