﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;
using System;

public class Player : MonoBehaviour
{
    public Wand wand;
    public GameObject spellPrefab;
    public Animator wandAnim;
    new public Transform camera;

    static Player instance;
    public static Player Instance { get { return instance; } }

    new Transform light;
    AudioSource audioSource;
    FirstPersonController fpc;
    CharacterController cc;
    Vector3 direction;
    Vector3 pos0;

    bool isLit;
    bool isBlinking;
    bool isfloating;
    bool peaked;
    bool slowing;
    float timeScale;
    float speed;
    float distance;
    float blinkSpeed;
    float timer;
    float force = 1;

    void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        camera = GetComponentInChildren<Camera>().transform;
        wand = camera.GetComponentInChildren<Wand>();
        wandAnim = wand.GetComponent<Animator>();
        spellPrefab = (GameObject)Resources.Load("Prefabs/Spell");
        fpc = GetComponent<FirstPersonController>();
        cc = GetComponent<CharacterController>();
        instance = this;
    }

    void Update()
    {
        if (slowing)
            SlowTime();
    }

    public void ShootSpell(string spellName, float speed)
    {
        Spell newSpell = Instantiate(spellPrefab).GetComponent<Spell>();
        newSpell.transform.SetParent(wand.transform);
        newSpell.transform.localPosition = wand.point.localPosition;
        newSpell.transform.forward = camera.forward;
        newSpell.Shoot(spellName, newSpell.transform.forward, speed);
    }

    public void LightWand()
    {
        GameObject lightObj = (GameObject)Instantiate(Resources.Load<GameObject>("Prefabs/WandLight"), wand.point.transform.position, Quaternion.identity);
        light = lightObj.transform;
        lightObj.AddComponent<LightSource>();
        light.SetParent(wand.transform);
        light.GetComponent<Light>().type = LightType.Spot;
        light.GetComponent<Light>().range *= force;
        light.forward = camera.forward;
    }

    void FixedUpdate()
    {
        if (isBlinking)
        {
            cc.Move(transform.forward * blinkSpeed * 10 * Time.deltaTime);
            if (cc.velocity == Vector3.zero)
                isBlinking = false;
            if (Time.time >= timer + blinkSpeed / 3)
            {
                isBlinking = false;
                timer = 0;
            }
        }
        if (isfloating)
            Float();
    }

    void SlowTime()
    {
        if (timeScale + timeScale / 10 * Time.deltaTime <= 1)
        {
            timeScale += timeScale / 10 * Time.deltaTime;
            Time.timeScale = timeScale;
        }
        else
        {
            slowing = false;
            Time.timeScale = 1;
        }
        Debug.Log(timeScale);
    }

    public void SlowTime(float f)
    {
        timeScale = 1 / (1 + f);
        slowing = true;
    }

    public void Blink(float force)
    {
        timer = Time.time;
        blinkSpeed = force;
        isBlinking = true;
        fpc.floating = false;
    }

    public void Float(float f)
    {
        pos0 = transform.position;
        force = f;
        speed = force * 3;
        direction = Vector3.up;
        distance = speed;
        isfloating = true;
        fpc.floating = true;
    }

    void Float()
    {
        if (!peaked && CanMoveY() && transform.position.y < pos0.y + distance)
        {
            cc.Move(direction * speed * Time.deltaTime);
        }
        else
        {
            if (!peaked)
            {
                timer = Time.time;
                peaked = true;
            }
            else if (Time.time >= timer + 2)
            {
                if (!cc.isGrounded)
                {
                    Vector3 dir = speed * Vector3.down;
                    cc.Move(dir * Time.deltaTime);
                }
                else
                {
                    isfloating = false;
                    fpc.floating = false;
                    FinishMovement();
                }
            }
        }
    }

    public void RemoveLight()
    {
        if (wand.GetComponentInChildren<Light>())
        {
            wandAnim.Play("Idle");
            Destroy(wand.GetComponentInChildren<Light>().gameObject);
            AudioManager.Play("cast_missile", audioSource, 1, 1f);
            isLit = false;
        }
    }

    public void ConjureLight(float force)
    {
        if (!isLit)
        {
            wandAnim.Play("WandLightOn");
            isLit = true;
            AudioManager.Play("lumos", 1, 1f / force);
        }
        else
        {
            Light light = wand.GetComponentInChildren<Light>();
            if (light.range != 5 * force)
            {
                light.range = 5 * force;
                AudioManager.Play("lumos", 1, 1f / force);
            }
        }
    }

    protected void FinishMovement()
    {
        peaked = false;
        timer = 0;
        force = 0;
    }

    bool CanMoveY()
    {
        return !Physics.Raycast(transform.position, direction, cc.bounds.extents.y + 0.1f);
    }
}

