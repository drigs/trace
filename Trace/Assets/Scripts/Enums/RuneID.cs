﻿public enum RuneID
{
    Ob, Non, Ti, Ra, Se, Mo, Con, Tra, Act, On, Tw, Th, Fo, Fi, Si, Pa, Pb
}